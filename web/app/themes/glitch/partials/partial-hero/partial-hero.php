<!-- PARTIAL GALLERY -->

<?php $img = get_sub_field('immagine'); ?>
<section class="partial_hero" style="background-image: url(<?php echo $img ?>)">
  <h1><?php the_sub_field("titolo"); ?></h1>
  <h2><?php the_sub_field("payoff"); ?></h2>
  <p><?php the_sub_field("descrizione"); ?></p>
  <a href="<?php the_sub_field("link_bottone"); ?>"><?php the_sub_field("nome_bottone"); ?></a>
</section>

<!-- /PARTIAL GALLERY -->
