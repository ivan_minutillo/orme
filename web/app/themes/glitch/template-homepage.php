<?php
/**
 * Template Name: Home Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>

    <?php
      if( have_rows('sezione') ): ?>
        <?php 
          $counter = 1;
          while ( have_rows('sezione') ) : the_row();
            switch(get_row_layout()) {
              case 'hero':
                include "partials/partial-hero/partial-hero.php";
                break;
              case 'gallery_foto':
                include "partials/partial-gallery-foto/partial-gallery-foto.php";
                break;
            }
          $counter++;
          endwhile;
        ?>
    <?php endif; ?>
<?php endwhile; ?>
