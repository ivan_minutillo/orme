<?php 
add_action( 'init', 'cptui_register_my_cpts' );
function cptui_register_my_cpts() {
    $labels = array(
        "name" => "Works",
        "singular_name" => "Work",
        );

    $args = array(
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "work", "with_front" => true ),
        "query_var" => true,
                
        "supports" => array( "title", "editor", "revisions", "thumbnail" ),     
        "taxonomies" => array( "post_tag" )
    );
    register_post_type( "work", $args );

    $labels = array(
        "name" => "Clients",
        "singular_name" => "Client",
        );

    $args = array(
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "client", "with_front" => true ),
        "query_var" => true,
                
        "supports" => array( "title", "editor", "revisions", "thumbnail" ),     
        "taxonomies" => array( "post_tag" )
    );
    register_post_type( "client", $args );

    $labels = array(
        "name" => "Personas",
        "singular_name" => "Persona",
        );

    $args = array(
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "persona", "with_front" => true ),
        "query_var" => true,
                
        "supports" => array( "title", "editor", "revisions", "thumbnail" ),     
        "taxonomies" => array( "post_tag" )
    );
    register_post_type( "persona", $args );

    $labels = array(
        "name" => "Quotes",
        "singular_name" => "Quote",
        );

    $args = array(
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "quote", "with_front" => true ),
        "query_var" => true,
                
        "supports" => array( "title", "editor", "revisions", "thumbnail" ),     
    );
    register_post_type( "quote", $args );

    $labels = array(
        "name" => "Galleries",
        "singular_name" => "Gallery",
        );

    $args = array(
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "gallery", "with_front" => true ),
        "query_var" => true,
                
        "supports" => array( "title", "editor", "thumbnail" ),      
        "taxonomies" => array( "post_tag" )
    );
    register_post_type( "gallery", $args );

    $labels = array(
        "name" => "Events",
        "singular_name" => "Event",
        );

    $args = array(
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "event", "with_front" => true ),
        "query_var" => true,
                
        "supports" => array( "title", "editor", "revisions", "thumbnail" ),     
        "taxonomies" => array( "post_tag" )
    );
    register_post_type( "event", $args );

// End of cptui_register_my_cpts()
}



?>